import express from 'express';
import cors from 'cors';
import createError from 'http-errors';
import bodyParser from 'body-parser';
import session from 'express-session';
import logger from 'morgan';
import APIRoutes from './server/api-routes';
import { AuthUtils } from './server/utils';
import AuthRoutes from './server/auth-routes';
// import PassportConfig from './server/config/passport';

require('./server/config/passport');

const app = express(); // setup express application

// Parse incoming requests data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Enable CORS Module
app.use(cors());

// A logger for all requests
app.use(logger('dev'));

app.use(
  session({
    secret: 'proyecto-upev',
    cookie: { maxAge: 60000 },
    resave: false,
    saveUninitialized: false
  })
);

// Use Authentication Routes
app.use(AuthRoutes);
// Use API Routes
app.use('/api', APIRoutes);
// app.use('/api', AuthUtils.required, APIRoutes);

// catch 404 and forward to error handler
// app.use((req, res, next) => {
//   next(createError(404));
// });

// // error handler
// // eslint-disable-next-line no-unused-vars
// app.use((err, req, res, next) => {
//   res.locals.message = err.message;
//   // set locals, only providing error in development
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   if (err.name === 'UnauthorizedError') {
//     res.status(401).send({
//       success: false,
//       errors: ['No estás autorizado a realizar esta acción. Por favor inicia sesión antes']
//     });
//   } else {
//     // Respond with error message
//     res.status(err.status || 500);
//     res.json({
//       success: false,
//       errors: [{ message: 'The requested route cannot be found' }]
//     });
//   }
// });

module.exports = app;
