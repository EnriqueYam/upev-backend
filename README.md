# UPEV Backend

Repositorio para la parte back-end del proyecto de Ingeniería de Software

## Comenzar

Instala Sequelize-CLI y Nodemon

```
npm install -g sequelize-cli nodemon
```

Descarga los últimos cambios al repo

```
git pull
```

Instala las dependencias del proyecto

```
npm install
```

Crea la base de datos y ejecuta las migraciones

```
sequelize db:create
sequelize db:migrate
```

Inicia el servidor en desarrollo

```
npm start
```

## Crear una nueva migración y modelo

Crea una nueva rama para tu trabajo `git checkout -b NombreDeLaTabla`

```
git checkout -b UnidadAprendizaje
```

Genera una nueva migración y modelo

```
sequelize model:create --name NombreDelModelo --attributes Atributo1:tipo,Atributo2:tipo
```

Para más referencias ver: http://docs.sequelizejs.com/manual/migrations.html#creating-first-model--and-migration-

### Modifica la migración generada por el CLI

Modifica el archivo de la migración y del modelo que fueron generados.

En `server/migrations/2019XXXXXXXXX-create-ud-aprendizaje.js`

```javascript
module.exports = {
  up: (queryInterface, Sequelize) => {
    /* 
      Modificar el nombre de la tabla para que corresponda al nombre del
      modelo en PLURAL
      Ejempo UnidadAprendizaje -> UnidadesAprendizaje
    */
    return queryInterface.createTable('UnidadesAprendizaje', {
      /* El nombre del id que corresponda a la tabla */
      idUnidadAprendizaje: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nombre: {
        type: Sequelize.STRING /* Ajustar para que corresponda con la tabla */,
        allowNull: false
      },
      /* Llave foránea */
      idPlanEstudios: {
        type: Sequelize.INTEGER
        /* 
          La tabla aún no existe por ello no se crea la referencia
          a la llave foranea 
        */

        // TODO: Enable this FK reference
        // references: {
        //   model: 'PlanesEstudio',
        //   key: 'idPlanEstudios'
        // },
        // onUpdate: 'CASCADE',
        // onDelete: 'CASCADE'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    /* 
      Modificar el nombre de la tabla para que corresponda al nombre del
      modelo en PLURAL
      Ejempo UnidadAprendizaje -> UnidadesAprendizaje
    */
    return queryInterface.dropTable('UnidadesAprendizaje');
  }
};
```

Renombra el archivo del modelo para que corresponda al nombre del modelo.

En `server/models/UnidadAprendizaje.js`

```javascript
module.exports = (sequelize, DataTypes) => {
  const UnidadAprendizaje = sequelize.define(
    /* 
      El nombre del modelo, en SINGULAR
      Debe corresponder al nombre de la constante de retorno (la que está en la línea de arriba)
     */
    'UnidadAprendizaje',
    {
      /* 
        El nombre del id conforme a la tabla
        Debe corresponder al nombre del id en la migración
       */
      idUnidadAprendizaje: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
      },
      nombre: {
        type: DataTypes.STRING,
        allowNull: false,
        /* 
          Validaciones
          Más info en http://docs.sequelizejs.com/manual/models-definition.html#per-attribute-validations
          Por ahora solamente las básicas hasta que estén los CU
         */
        validate: {
          notEmpty: true,
          len: [2, 50]
        }
      },
      /* 
        Llave foránea
        No hace falta hacer la referencia a la llave foránea aquí
       */
      idPlanEstudios: {
        type: DataTypes.INTEGER
      }
    },
    {
      /*
        El nombre de la tabla en la BD 
        Debe corresponder con el nombre dado en la migración (de nuevo el PLURAL del nombre del modelo)
      */
      tableName: 'UnidadesAprendizaje',
      sequelize
    }
  );
  UnidadAprendizaje.associate = models => {
    /* 
      Relaciones entre modelos
     */
    UnidadAprendizaje.hasMany(models.Solicitud, {
      /* Establece una relación 1 a N con Solicitud */
      foreignKey: 'idUnidadAcademica',
      /* 
        Cómo se va a nombrar al campo (la relación) cuando se tome desde la BD
        Debe corresponder al nombre del modelo en plural y en minúsculas
       */
      as: 'solicitudes',
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE'
    });
  };
  return UnidadAprendizaje;
};
```

Ejecuta la migración `sequelize db:migrate`. Los cambios deberián verse reflejados en la BD.

## Agrega un Controlador para el nuevo modelo

En `server/controllers/UnidadAprendizajeController.js`. Crea las principales acciones para el CRUD:

- **Controller.list**: Obtiene todos los registros de la base de datos para ese modelo
- **Controller.create**: Crea un nuevo registro en la base de datos con los parámetros tomados en el body de la petición
- **Controller.show**: Obtiene un registro de la base de datos con base en los parámetros tomados en el url de la petición (por ahora el id)
- **Controller.update**: Actualiza un registro en la base de datos con base en los parámetros tomados en la url de la petición (por ahora el id) utilizando los datos en el body de la petición
- **Controller.delete**: Elimina un registro de la base de datos con base en los parámetros tomados en el url de la petición (por ahora el id)

```javascript
/* Importa el modelo que acabas de crear */
import { UnidadAprendizaje } from '../models';
/* Para responder y manejar los errores */
import ControllerUtils from '../utils/ControllerUtils';

/* 
  Nombrar la clase con el nombre del modelo más 'Controller'
 */
class UnidadAprendizajeController {
  /**
   * Fetches all `Unidades Académicas` from database and gets them back to the client
   * @param {Request} req The request from the client
   * @param {Response} res Response to the client
   */
  static list(req, res) {
    // Fetch all `Unidades Académicas` stored on DB
    return UnidadAprendizaje.findAll().then(udsAprendizaje =>
      ControllerUtils.successfulOperation(res, {
        data: udsAprendizaje
      })
    );
  }

  /**
   * Creates a new `Unidad Académica` taking the parameters from the request body
   * @param {Request} req The request from the client
   * @param {Response} res Response to the client
   */
  static async create(req, res) {
    const { nombre, idPlanEstudios } = req.body;

    try {
      const nuevaUnidadAprendizaje = await UnidadAprendizaje.create({
        nombre,
        idPlanEstudios
      });

      ControllerUtils.successfulInsertion(res, {
        message: `La unidad de aprendizaje ${nombre} se creó correctamente`,
        data: nuevaUnidadAprendizaje
      });
    } catch (error) {
      ControllerUtils.handleError(res, error);
    }
  }

  static async show(req, res) {
    const { id } = req.params;

    const udAprendizaje = await UnidadAprendizaje.findOne({
      where: {
        idUnidadAprendizaje: id
      },
      /* 
        Debe corresponder al nombre de la asociación definida en el modelo
       */
      include: ['solicitudes']
    });

    if (!udAprendizaje) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la unidad de aprendizaje con el id: ${id}`
      });

      return;
    }

    ControllerUtils.successfulOperation(res, {
      data: udAprendizaje
    });
  }

  static async update(req, res) {
    const { id } = req.params;

    const udAprendizaje = await UnidadAprendizaje.findByPk(id);

    if (!udAprendizaje) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la unidad de aprendizaje con el id: ${id}`
      });

      return;
    }

    const { nombre, idPlanEstudios } = req.body;

    try {
      const udAprendizajeActualizada = await udAprendizaje.update({
        nombre,
        idPlanEstudios
      });

      ControllerUtils.successfulOperation(res, {
        data: udAprendizajeActualizada
      });
    } catch (error) {
      // Error when updating `Unidad Aprendizaje`
      ControllerUtils.handleError(res, error);
    }
  }

  static async delete(req, res) {
    const { id } = req.params;

    const udAprendizaje = await UnidadAprendizaje.findByPk(id);

    if (!udAprendizaje) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la unidad académica con el id: ${id}`
      });

      return;
    }

    try {
      await udAprendizaje.destroy();

      ControllerUtils.successfulOperation(res, {
        message: `La unidad de aprendizaje ${udAprendizaje.nombre} se eliminó correctamente`
      });
    } catch (error) {
      // Error when deleting `Unidad Aprendizaje`
      ControllerUtils.handleError(res, error);
    }
  }
}

export default UnidadAprendizajeController;
```

## Agrega las Rutas a la nueva Entidad

En `server/routes.js`

```javascript
import { Router } from 'express';
import UnidadAcademicaController from './controllers/UnidadAcademicaController';
/* 
  Importa el controloador que acabas de crear
 */
import UnidadAprendizajeController from './controllers/UnidadAprendizajeController';
import SolicitudController from './controllers/SolicitudController';

const routes = Router();

// Unidad Academica Routes
const UnidadAcademicaRoute = '/unidades-academicas';
routes.get(UnidadAcademicaRoute, UnidadAcademicaController.list);
routes.post(UnidadAcademicaRoute, UnidadAcademicaController.create);
routes.get(`${UnidadAcademicaRoute}/:id`, UnidadAcademicaController.show);
routes.put(`${UnidadAcademicaRoute}/:id`, UnidadAcademicaController.update);
routes.delete(`${UnidadAcademicaRoute}/:id`, UnidadAcademicaController.delete);

/* 
  Define las rutas para el nuevo controlador
 */
// Unidad Aprendizaje Routes
/* 
  El nombre de la ruta debe corresponder al nombre en PLURAL del modelo
  utilizando guiones ('-') en lugar de espacios
 */
const UnidadAprendizajeRoute = '/unidades-aprendizaje';
routes.get(UnidadAprendizajeRoute, UnidadAprendizajeController.list);
routes.post(UnidadAprendizajeRoute, UnidadAprendizajeController.create);
/* 
  El nombre del placeholder (id en este caso) debe corresponder al nombre
  utilizado en la acción del controlador
  const { id } = req.params;
 */
routes.get(`${UnidadAprendizajeRoute}/:id`, UnidadAprendizajeController.show);
/* 
  La acción UPDATE corresponde a una petición tipo PUT/PATCH
 */
routes.put(`${UnidadAprendizajeRoute}/:id`, UnidadAprendizajeController.update);
/* 
  La acción UPDATE corresponde a una petición tipo DELETE
 */
routes.delete(`${UnidadAprendizajeRoute}/:id`, UnidadAprendizajeController.delete);

// Solicitud Routes
const SolicitudRoute = '/solicitudes';
routes.get(SolicitudRoute, SolicitudController.list);
routes.post(SolicitudRoute, SolicitudController.create);
routes.get(`${SolicitudRoute}/:id`, SolicitudController.show);
routes.put(`${SolicitudRoute}/:id`, SolicitudController.update);
routes.delete(`${SolicitudRoute}/:id`, SolicitudController.delete);

module.exports = routes;
```

## Prueba que las rutas funcionen de manera correcta

## Guarda los cambios en tu rama

```
git add -A
git commit -m "Complete UnidadAprendizaje CRUD operations"
```

Sube tu rama al repo: `git push origin ElNombreDeTuRama`

```
git push origin UnidadAprendizaje
```

## Crea una nueva Merge Request

Instrucciones en: https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html

## Avisa a los responsables de tus cambios

Por whats xD
