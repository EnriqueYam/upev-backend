import { Responsable } from '../models';
import { ControllerUtils } from '../utils';

class ResponsableController {
  static async list(req, res) {
    // Fetch all 'Responsable' stored on DB
    try {
      const responsables = await Responsable.findAll(req.query.sql);

      ControllerUtils.successfulOperation(res, {
        data: responsables
      });
    } catch (error) {
      ControllerUtils.handleError(res, error, {
        message: 'Ocurrió un error al ejecutar la consulta'
      });
    }
  }

  static async create(req, res) {
    const {
      nombre,
      puesto,
      extension,
      celular,
      correoInstitucional,
      correoAlternativo,
      tipo
    } = req.body;
    try {
      const nuevoResponsable = await Responsable.create({
        nombre,
        puesto,
        extension,
        celular,
        correoInstitucional,
        correoAlternativo,
        tipo
      });

      ControllerUtils.successfulInsertion(res, {
        message: `El responsable con  ${nombre} se registro correctamente`,
        data: nuevoResponsable
      });
    } catch (error) {
      ControllerUtils.handleError(res, error);
    }
  }

  static async show(req, res) {
    const { id } = req.params;

    const responsable = await Responsable.findOne({
      where: {
        idResponsable: id
      },
      // Include its relationships
      include: ['solicitudes']
    });

    if (!responsable) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró ningun Responsable con el id: ${id}`
      });

      return;
    }

    ControllerUtils.successfulOperation(res, {
      data: responsable
    });
  }

  static async update(req, res) {
    const { id } = req.params;

    const responsable = await Responsable.findByPk(id);

    if (!responsable) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró responsable con el id: ${id}`
      });

      return;
    }

    const {
      nombre,
      puesto,
      extension,
      celular,
      correoInstitucional,
      correoAlternativo,
      tipo
    } = req.body;

    try {
      const responsableActualizado = await responsable.update({
        nombre,
        puesto,
        extension,
        celular,
        correoInstitucional,
        correoAlternativo,
        tipo
      });

      ControllerUtils.successfulOperation(res, {
        data: responsableActualizado
      });
    } catch (error) {
      // Error when updating `Responsable`
      ControllerUtils.handleError(res, error);
    }
  }

  static async delete(req, res) {
    const { id } = req.params;

    const responsable = await Responsable.findByPk(id);

    if (!responsable) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró responsable con el id: ${id}`
      });

      return;
    }

    try {
      await responsable.destroy();

      ControllerUtils.successfulOperation(res, {
        message: `El responsable con el nombre ${responsable.nombre} se eliminó correctamente`
      });
    } catch (error) {
      // Error when deleting `Unidad Académica`
      ControllerUtils.handleError(res, error);
    }
  }
}
export default ResponsableController;
