import { PlanEstudios } from '../models';
import { ControllerUtils } from '../utils';

class PlanEstudiosController {
  /**
   * Fetches all `Planes de Estudios` from database and gets them back to the client
   * @param {Request} req The request from the client
   * @param {Response} res Response to the client
   */
  static async list(req, res) {
    // Fetch all `Planes de Estudios` stored on DB
    try {
      const planesEstudio = await PlanEstudios.findAll(req.query.sql);

      ControllerUtils.successfulOperation(res, {
        data: planesEstudio
      });
    } catch (error) {
      ControllerUtils.handleError(res, error, {
        message: 'Ocurrió un error al ejecutar la consulta'
      });
    }
  }

  /**
   * Creates a new `Planes de Estudios` taking the parameters from the request body
   * @param {Request} req The request from the client
   * @param {Response} res Response to the client
   */
  static async create(req, res) {
    const { version } = req.body;

    try {
      const nuevoPlanEstudios = await PlanEstudios.create({
        version
      });

      ControllerUtils.successfulInsertion(res, {
        message: `El plan de estudios ${version} se creó correctamente`,
        data: nuevoPlanEstudios
      });
    } catch (error) {
      ControllerUtils.handleError(res, error);
    }
  }

  static async show(req, res) {
    const { id } = req.params;

    const planEstudios = await PlanEstudios.findOne({
      where: {
        idPlanEstudios: id
      },
      // Include its relationships
      include: ['programasAcademicos', 'unidadesAprendizaje', 'unidadesAcademicas']
    });

    if (!planEstudios) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró algun plan de estudios con el id: ${id}`
      });

      return;
    }

    ControllerUtils.successfulOperation(res, {
      data: planEstudios
    });
  }

  static async update(req, res) {
    const { id } = req.params;

    const planEstudios = await PlanEstudios.findByPk(id);

    if (!planEstudios) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró algun plan de estudios con el id: ${id}`
      });

      return;
    }

    const { version } = req.body;

    try {
      const planEstudiosActualizado = await PlanEstudios.update({
        version
      });

      ControllerUtils.successfulOperation(res, {
        data: planEstudiosActualizado
      });
    } catch (error) {
      // Error when updating `Plan De Estudios`
      ControllerUtils.handleError(res, error);
    }
  }

  static async delete(req, res) {
    const { id } = req.params;

    const planEstudios = await PlanEstudios.findByPk(id);

    if (!planEstudios) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró algun plan de estudios con el id: ${id}`
      });

      return;
    }

    try {
      await planEstudios.destroy();

      ControllerUtils.successfulOperation(res, {
        message: `El plan de estudios ${planEstudios.version} se eliminó correctamente`
      });
    } catch (error) {
      // Error when deleting `Plan De Estudios`
      ControllerUtils.handleError(res, error);
    }
  }
}

export default PlanEstudiosController;
