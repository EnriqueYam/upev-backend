import { Solicitud, Responsable, sequelize } from '../models';
import { ControllerUtils } from '../utils';

/**
 *
 * @param {Object} nuevaSolicitud La solicitud que debe crearse
 * @param {Array} responsablesNuevos Los responsables que serán agregados y relacionados a la nueva solicitud
 * @param {Array<Number>} idsResponsablesExistentes Arreglo con ids de responsables que ya existen y solamente serán \
 * relacionados a la nueva solicitud
 * @throws
 */
async function crearConResponsablesYAsociar(
  nuevaSolicitud,
  responsablesNuevos,
  idsResponsablesExistentes
) {
  // TODO: let create with no responsablesNuevos o responsablesExistentes
  // Obtener una transacción
  let transaccion;

  try {
    transaccion = await sequelize.transaction();

    const solicitudCreada = await Solicitud.create(nuevaSolicitud, {
      transaction: transaccion
    });
    // Insertar todos los nuevos responsables
    const responsablesCreados = await Responsable.bulkCreate(responsablesNuevos, {
      transaction: transaccion,
      validate: true,
      // TODO: if failure check this
      returning: true
    });

    const responsablesConsultados = await Responsable.findAll({
      where: {
        idResponsable: idsResponsablesExistentes
      },
      transaction: transaccion
    });

    const relaciones = await solicitudCreada.setResponsables(
      // Establecer la relación entre la solicitud a crear y los reponsables
      responsablesCreados.concat(responsablesConsultados),
      {
        transaction: transaccion
      }
    );

    await transaccion.commit();

    return {
      solicitud: solicitudCreada,
      relaciones,
      nuevosResponsables: responsablesCreados,
      responsablesExistentes: responsablesConsultados
    };
  } catch (error) {
    // Falló la transacción, hacer rollback
    await transaccion.rollback();
    throw error;
  }
}

class SolicitudController {
  /**
   * Fetches all `Solicitudes` from database and gets them back to the client
   * @param {Request} req The request from the client
   * @param {Response} res Response to the client
   */
  static async list(req, res) {
    // Fetch all `Solicitudes` stored on DB
    try {
      const solicitudes = await Solicitud.findAll(req.query.sql);

      ControllerUtils.successfulOperation(res, {
        data: solicitudes
      });
    } catch (error) {
      ControllerUtils.handleError(res, error, {
        message: 'Ocurrió un error al ejecutar la consulta'
      });
    }
  }

  /**
   * Creates a new `Solicitud` taking the parameters from the request body
   * @param {Request} req The request from the client
   * @param {Response} res Response to the client
   */
  static async create(req, res) {
    const {
      descripcion,
      CADIDO,
      numeroOficio,
      folio,
      cedulaRegistro,
      planeacionDidactica,
      oficioSolicitud,
      dictamenAcademico,
      idPlataforma,
      reporteOriginalidad,
      reporteEvaluacion,
      solicitudEvaluacion,
      reporteTecPed,
      unidadAcademica,
      idResponsable,
      idUnidadAprendizaje
    } = req.body;

    try {
      const nuevaSolicitud = await Solicitud.create({
        descripcion,
        CADIDO,
        folio,
        numeroOficio,
        cedulaRegistro,
        planeacionDidactica,
        oficioSolicitud,
        dictamenAcademico,
        idPlataforma,
        reporteOriginalidad,
        reporteEvaluacion,
        solicitudEvaluacion,
        reporteTecPed,
        unidadAcademica,
        idResponsable,
        idUnidadAprendizaje
      });

      ControllerUtils.successfulInsertion(res, {
        data: nuevaSolicitud
      });
    } catch (error) {
      ControllerUtils.handleError(res, error);
    }
  }

  static async createWithResponsables(req, res) {
    const { solicitud, responsablesNuevos, responsablesExistentes } = req.body;
    try {
      const resultados = await crearConResponsablesYAsociar(
        solicitud,
        responsablesNuevos,
        responsablesExistentes
      );
      ControllerUtils.successfulInsertion(res, {
        data: resultados
      });
    } catch (error) {
      ControllerUtils.handleError(res, error);
    }
  }

  static async show(req, res) {
    const { id } = req.params;
    const solicitud = await Solicitud.findOne({
      where: {
        idSolicitud: id
      },
      // Include its relationships
      include: ['unidadAprendizaje', 'responsable', 'responsable', 'distribuciones']
    });

    if (!solicitud) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la solicitud con el id: ${id}`
      });

      return;
    }

    ControllerUtils.successfulOperation(res, {
      data: solicitud
    });
  }

  static async update(req, res) {
    const { id } = req.params;

    const solicitud = await Solicitud.findByPk(id);

    if (!solicitud) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la solicitud con el id: ${id}`
      });

      return;
    }

    const {
      descripcion,
      CADIDO,
      folio,
      numeroOficio,
      estado,
      cedulaRegistro,
      planeacionDidactica,
      oficioSolicitud,
      dictamenAcademico,
      idPlataforma,
      reporteOriginalidad,
      reporteEvaluacion,
      solicitudEvaluacion,
      reporteTecPed,
      unidadAcademica,
      idResponsable,
      idUnidadAprendizaje
    } = req.body;

    try {
      const solicitudActualizada = await solicitud.update({
        descripcion,
        CADIDO,
        folio,
        numeroOficio,
        estado,
        cedulaRegistro,
        planeacionDidactica,
        oficioSolicitud,
        dictamenAcademico,
        idPlataforma,
        reporteOriginalidad,
        reporteEvaluacion,
        solicitudEvaluacion,
        reporteTecPed,
        unidadAcademica,
        idResponsable,
        idUnidadAprendizaje
      });

      ControllerUtils.successfulOperation(res, {
        data: solicitudActualizada
      });
    } catch (error) {
      // Error when updating `Solicitud`
      ControllerUtils.handleError(res, error);
    }
  }

  static async delete(req, res) {
    const { id } = req.params;

    const solicitud = await Solicitud.findByPk(id);

    if (!solicitud) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la unidad académica con el id: ${id}`
      });

      return;
    }

    try {
      await solicitud.destroy();

      ControllerUtils.successfulOperation(res, {
        message: `La unidad académica ${solicitud.nombre} se eliminó correctamente`
      });
    } catch (error) {
      // Error when deleting `Solicitud`
      ControllerUtils.handleError(res, error);
    }
  }
}

export default SolicitudController;
