import { ProgramaAcademico } from '../models';
import { ControllerUtils } from '../utils';

class ProgramaAcademicoController {
  /**
   * Fetches all `Programas Académicos` from database and gets them back to the client
   * @param {Request} req The request from the client
   * @param {Response} res Response to the client
   */
  static async list(req, res) {
    // Fetch all `Programas Académicos` stored on DB
    try {
      const programasAcademicos = await ProgramaAcademico.findAll(req.query.sql);

      ControllerUtils.successfulOperation(res, {
        data: programasAcademicos
      });
    } catch (error) {
      ControllerUtils.handleError(res, error, {
        message: 'Ocurrió un error al ejecutar la consulta'
      });
    }
  }

  /**
   * Creates a new `Programa Académico` taking the parameters from the request body
   * @param {Request} req The request from the client
   * @param {Response} res Response to the client
   */
  static async create(req, res) {
    const { nombre, idPlanEstudios, idUnidadAcademica } = req.body;

    const compare = await ProgramaAcademico.findOne({
      where: {
        nombre
      }
    });
    if (compare) {
      return res.sendStatus(412);
    }
    try {
      const nuevoProgAcademico = await ProgramaAcademico.create({
        nombre,
        idPlanEstudios,
        idUnidadAcademica
      });

      ControllerUtils.successfulInsertion(res, {
        message: `El programa academico: ${nombre} se creó correctamente`,
        data: nuevoProgAcademico
      });
    } catch (error) {
      ControllerUtils.handleError(res, error);
    }
  }

  static async show(req, res) {
    const { id } = req.params;

    const programaAcademico = await ProgramaAcademico.findOne({
      where: {
        idProgramaAcademico: id
      },
      // Include its relationships
      include: ['planEstudios', 'unidadAcademica']
    });

    if (!programaAcademico) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró el programa academico con el id: ${id}`
      });

      return;
    }

    ControllerUtils.successfulOperation(res, {
      data: programaAcademico
    });
  }

  static async update(req, res) {
    const { id } = req.params;

    const programaAcademico = await ProgramaAcademico.findByPk(id);

    if (!programaAcademico) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la el programa academico con el id: ${id}`
      });

      return;
    }

    const { nombre, idPlanEstudios, idUnidadAcademica } = req.body;

    try {
      const updateProgAcademico = await programaAcademico.update({
        nombre,
        idPlanEstudios,
        idUnidadAcademica
      });

      ControllerUtils.successfulOperation(res, {
        data: updateProgAcademico
      });
    } catch (error) {
      // Error when updating `Unidad Aprendizaje`
      ControllerUtils.handleError(res, error);
    }
  }

  static async delete(req, res) {
    const { id } = req.params;

    const programaAcademico = await ProgramaAcademico.findByPk(id);

    if (!programaAcademico) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró el programa academico con el id: ${id}`
      });

      return;
    }

    try {
      await programaAcademico.destroy();

      ControllerUtils.successfulOperation(res, {
        message: `El programa academico: ${programaAcademico.nombre} se eliminó correctamente`
      });
    } catch (error) {
      // Error when deleting `Unidad Aprendizaje`
      ControllerUtils.handleError(res, error);
    }
  }
}

export default ProgramaAcademicoController;
