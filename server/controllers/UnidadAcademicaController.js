// import { UnidadAcademica } from '../models';
import { UnidadAcademica } from '../models';
import { ControllerUtils } from '../utils';

class UnidadAcademicaController {
  /**
   * Fetches all `Unidades Académicas` from database and gets them back to the client
   * @param {Request} req The request from the client
   * @param {Response} res Response to the client
   */
  static async list(req, res) {
    // Fetch all `Unidades Académicas` stored on DB
    try {
      const unidadesAcademicas = await UnidadAcademica.findAll(req.query.sql);

      ControllerUtils.successfulOperation(res, {
        data: unidadesAcademicas
      });
    } catch (error) {
      ControllerUtils.handleError(res, error, {
        message: 'Ocurrió un error al ejecutar la consulta'
      });
    }
  }

  /**
   * Creates a new `Unidad Académica` taking the parameters from the request body
   * @param {Request} req The request from the client
   * @param {Response} res Response to the client
   */
  static async create(req, res) {
    const { nombre, titular } = req.body;

    try {
      const nuevaUnidadAcademica = await UnidadAcademica.create({
        nombre,
        titular
      });

      ControllerUtils.successfulInsertion(res, {
        message: `La unidad académica ${nombre} se creó correctamente`,
        data: nuevaUnidadAcademica
      });
    } catch (error) {
      ControllerUtils.handleError(res, error);
    }
  }

  static async show(req, res) {
    const { id } = req.params;

    const unidadAcademica = await UnidadAcademica.findOne({
      where: {
        idUnidadAcademica: id
      },
      // Include its relationships
      include: ['programasAcademicos']
    });

    if (!unidadAcademica) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la unidad académica con el id: ${id}`
      });

      return;
    }

    ControllerUtils.successfulOperation(res, {
      data: unidadAcademica
    });
  }

  static async update(req, res) {
    const { id } = req.params;

    const unidadAcademica = await UnidadAcademica.findByPk(id);

    if (!unidadAcademica) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la unidad académica con el id: ${id}`
      });

      return;
    }

    const { nombre, titular, programasAcademicos } = req.body;

    try {
      const unidadAcademicaActualizada = await unidadAcademica.update({
        nombre,
        titular,
        programasAcademicos
      });

      ControllerUtils.successfulOperation(res, {
        data: unidadAcademicaActualizada
      });
    } catch (error) {
      // Error when updating `Unidad Académica`
      ControllerUtils.handleError(res, error);
    }
  }

  static async delete(req, res) {
    const { id } = req.params;

    const unidadAcademica = await UnidadAcademica.findByPk(id);

    if (!unidadAcademica) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la unidad académica con el id: ${id}`
      });

      return;
    }

    try {
      await unidadAcademica.destroy();

      ControllerUtils.successfulOperation(res, {
        message: `La unidad académica ${unidadAcademica.nombre} se eliminó correctamente`
      });
    } catch (error) {
      // Error when deleting `Unidad Académica`
      ControllerUtils.handleError(res, error);
    }
  }
}

export default UnidadAcademicaController;
