import { Turnado } from '../models';
import { ControllerUtils } from '../utils';

class TurnadoController {
  /**
   * Fetches all `Turnados` from database and gets them back to the client
   * @param {Request} req The request from the client
   * @param {Response} res Response to the client
   */
  static async list(req, res) {
    // Fetch all `Turnados` stored on DB
    try {
      const turnados = await Turnado.findAll(req.query.sql);

      ControllerUtils.successfulOperation(res, {
        data: turnados
      });
    } catch (error) {
      ControllerUtils.handleError(res, error, {
        message: 'Ocurrió un error al ejecutar la consulta'
      });
    }
  }

  /**
   * Creates a new `Turnado` taking the parameters from the request body
   * @param {Request} req The request from the client
   * @param {Response} res Response to the client
   */
  static async create(req, res) {
    const { estado, tiempo, idAreaUPEV } = req.body;

    try {
      const nuevoTurnado = await Turnado.create({
        estado,
        tiempo,
        idAreaUPEV
      });

      ControllerUtils.successfulInsertion(res, {
        message: `El turnado se creó correctamente`,
        data: nuevoTurnado
      });
    } catch (error) {
      ControllerUtils.handleError(res, error);
    }
  }

  static async show(req, res) {
    const { id } = req.params;

    const turnado = await Turnado.findOne({
      where: {
        idTurnado: id
      },
      // Include its relationships
      include: ['areaUPEV', 'distribuciones']
    });

    if (!turnado) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró el turnado con el id: ${id}`
      });

      return;
    }

    ControllerUtils.successfulOperation(res, {
      data: turnado
    });
  }

  static async update(req, res) {
    const { id } = req.params;

    const turnado = await Turnado.findByPk(id);

    if (!turnado) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró el turnado con el id: ${id}`
      });

      return;
    }

    const { estado, tiempo, idAreaUPEV } = req.body;

    try {
      const turnadoActualizado = await turnado.update({
        estado,
        tiempo,
        idAreaUPEV
      });

      ControllerUtils.successfulOperation(res, {
        data: turnadoActualizado
      });
    } catch (error) {
      // Error when updating `Turnado`
      ControllerUtils.handleError(res, error);
    }
  }

  static async delete(req, res) {
    const { id } = req.params;

    const turnado = await Turnado.findByPk(id);

    if (!turnado) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró el turnado con el id: ${id}`
      });

      return;
    }

    try {
      await turnado.destroy();

      ControllerUtils.successfulOperation(res, {
        message: `El turnado se eliminó correctamente`
      });
    } catch (error) {
      // Error when deleting `Turnado`
      ControllerUtils.handleError(res, error);
    }
  }
}

export default TurnadoController;
