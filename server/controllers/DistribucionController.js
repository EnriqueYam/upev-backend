import { Distribucion } from '../models';
import { ControllerUtils } from '../utils';

class DistribucionController {
  static async create(req, res) {
    const { idSolicitud, idTurnado } = req.body;
    try {
      const distribucion = await Distribucion.create({
        idSolicitud,
        idTurnado
      });

      ControllerUtils.successfulInsertion(res, {
        message: `El turnado se creó correctamente`,
        data: distribucion
      });
    } catch (error) {
      ControllerUtils.handleError(res, error);
    }
  }
}

export default DistribucionController;
