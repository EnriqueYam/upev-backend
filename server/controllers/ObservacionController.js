import { Observacion } from '../models';
import { ControllerUtils } from '../utils';

class ObservacionController {
  /**
   * Fetches all `Programas Académicos` from database and gets them back to the client
   * @param {Request} req The request from the client
   * @param {Response} res Response to the client
   */
  static async list(req, res) {
    // Fetch all `Programas Académicos` stored on DB
    try {
      const observaciones = await Observacion.findAll(req.query.sql);

      ControllerUtils.successfulOperation(res, {
        data: observaciones
      });
    } catch (error) {
      ControllerUtils.handleError(res, error, {
        message: 'Ocurrió un error al ejecutar la consulta'
      });
    }
  }

  /**
   * Creates a new `Programa Académico` taking the parameters from the request body
   * @param {Request} req The request from the client
   * @param {Response} res Response to the client
   */
  static async create(req, res) {
    const { descripcion, estado, fecha, idDistribucion } = req.body;

    try {
      const nuevaObservacion = await Observacion.create({
        descripcion,
        estado,
        fecha,
        idDistribucion
      });

      ControllerUtils.successfulInsertion(res, {
        message: `Se ha registrado una nueva Observacion`,
        data: nuevaObservacion
      });
    } catch (error) {
      ControllerUtils.handleError(res, error);
    }
  }

  static async showOneObservation(req, res) {
    const { idDistribucion } = req.params;

    const observacion = await Observacion.findAll({
      where: {
        idDistribucion
      },
      include: ['distribucion']
    });

    if (!observacion) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la Observación con el id: ${idDistribucion}`
      });

      return;
    }

    ControllerUtils.successfulOperation(res, {
      data: observacion
    });
  }

  static async show(req, res) {
    const { id } = req.params;

    const observacion = await Observacion.findOne({
      where: {
        idObservacion: id
      },
      // Include its relationships
      include: ['distribucion']
    });

    if (!observacion) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la Observación con el id: ${id}`
      });

      return;
    }

    ControllerUtils.successfulOperation(res, {
      data: observacion
    });
  }

  static async updateRespuesta(req, res) {
    const { id } = req.params;

    const observacion = await Observacion.findByPk(id);

    if (!observacion) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la Observación con el id: ${id}`
      });

      return;
    }

    const { fechaRespuesta, estado, respuesta } = req.body;

    try {
      const updateObservacion = await observacion.update({
        fechaRespuesta,
        estado,
        respuesta
      });

      ControllerUtils.successfulOperation(res, {
        data: updateObservacion
      });
    } catch (err) {
      ControllerUtils.handleError(res, err);
    }
  }

  static async update(req, res) {
    const { id } = req.params;

    const observacion = await Observacion.findByPk(id);

    if (!observacion) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la Observación con el id: ${id}`
      });

      return;
    }

    const { descripcion, estado, fecha, idDistribucion } = req.body;

    try {
      const updateObservacion = await observacion.update({
        descripcion,
        estado,
        fecha,
        idDistribucion
      });

      ControllerUtils.successfulOperation(res, {
        data: updateObservacion
      });
    } catch (error) {
      // Error when updating `Unidad Aprendizaje`
      ControllerUtils.handleError(res, error);
    }
  }

  static async delete(req, res) {
    const { id } = req.params;

    const observacion = await Observacion.findByPk(id);

    if (!observacion) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la Observación con el id: ${id}`
      });

      return;
    }

    try {
      await observacion.destroy();

      ControllerUtils.successfulOperation(res, {
        message: `La Observación correspondiente a la siguiente fecha: ${
          observacion.fecha
        } se eliminó correctamente`
      });
    } catch (error) {
      // Error when deleting `Unidad Aprendizaje`
      ControllerUtils.handleError(res, error);
    }
  }
}

export default ObservacionController;
