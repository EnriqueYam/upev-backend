import Passport from 'passport';
import { Usuario } from '../models';
import { ControllerUtils } from '../utils';

class UsuarioController {
  static async list(req, res) {
    try {
      const usuarios = await Usuario.findAll(req.query.sql);

      ControllerUtils.successfulOperation(res, {
        data: usuarios
      });
    } catch (err) {
      ControllerUtils.handleError(res, err, {
        message: 'Ocurrio un error al ejecutar la consulta'
      });
    }
  }

  static async create(req, res) {
    const { nombre, password, idUnidadAcademica, correo, idRol, idAreaUPEV } = req.body;

    try {
      const nuevoUsuario = await Usuario.create({
        nombre,
        password,
        idUnidadAcademica,
        correo,
        idRol,
        idAreaUPEV
      });

      await nuevoUsuario.hashPassword(password);
      const usuarioNuevaContrasena = await nuevoUsuario.save();

      ControllerUtils.successfulInsertion(res, {
        message: 'Se ha creado correctamente al usuario',
        data: {
          usuario: usuarioNuevaContrasena,
          token: usuarioNuevaContrasena.generateJWT()
        }
      });
    } catch (err) {
      ControllerUtils.handleError(res, err);
    }
  }

  static authenticate(req, res, next) {
    try {
      const { correo, password } = req.body;

      if (!correo || correo === '') {
        return res.status(422).send({
          success: false,
          errors: ['El correo es requerido para poder iniciar sesión']
        });
      }

      if (!password || password === '') {
        return res.status(422).send({
          success: false,
          errors: ['La contraseña es requerida para poder iniciar sesión']
        });
      }

      return Passport.authenticate('local', { session: false }, (error, user, info) => {
        if (error) {
          return next(error);
        }

        if (user) {
          return ControllerUtils.successfulOperation(res, {
            data: {
              usuario: user,
              token: user.generateJWT()
            }
          });
        }

        return ControllerUtils.notFoundError(res, info);
      })(req, res, next);
    } catch (error) {
      return ControllerUtils.handleError(res, error);
    }
  }

  static async current(req, res) {
    const { id } = req.payload;

    const usuarioActual = await Usuario.findOne({
      where: {
        idUsuario: id
      },
      include: ['unidadAcademica']
    });

    if (!usuarioActual) {
      ControllerUtils.notFoundError(res, {
        message: `No se al usuario con el id: ${id}`
      });

      return;
    }

    ControllerUtils.successfulOperation(res, {
      data: {
        usuario: usuarioActual,
        token: usuarioActual.generateJWT()
      }
    });
  }
}

export default UsuarioController;
