import { UnidadAprendizaje } from '../models';
import { ControllerUtils } from '../utils';

class UnidadAprendizajeController {
  /**
   * Fetches all `Unidades Académicas` from database and gets them back to the client
   * @param {Request} req The request from the client
   * @param {Response} res Response to the client
   */
  static async list(req, res) {
    // Fetch all `Unidades Académicas` stored on DB
    try {
      const unidadesAprendizaje = await UnidadAprendizaje.findAll(req.query.sql);

      ControllerUtils.successfulOperation(res, {
        data: unidadesAprendizaje
      });
    } catch (error) {
      ControllerUtils.handleError(res, error, {
        message: 'Ocurrió un error al ejecutar la consulta'
      });
    }
  }

  /**
   * Creates a new `Unidad Académica` taking the parameters from the request body
   * @param {Request} req The request from the client
   * @param {Response} res Response to the client
   */
  static async create(req, res) {
    const { nombre, idPlanEstudios } = req.body;

    try {
      const nuevaUnidadAprendizaje = await UnidadAprendizaje.create({
        nombre,
        idPlanEstudios
      });

      ControllerUtils.successfulInsertion(res, {
        message: `La unidad de aprendizaje ${nombre} se creó correctamente`,
        data: nuevaUnidadAprendizaje
      });
    } catch (error) {
      ControllerUtils.handleError(res, error);
    }
  }

  static async show(req, res) {
    const { id } = req.params;

    const unidadAprendizaje = await UnidadAprendizaje.findOne({
      where: {
        idUnidadAprendizaje: id
      },
      // Include its relationships
      include: ['planEstudios', 'solicitudes']
    });

    if (!unidadAprendizaje) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la unidad de aprendizaje con el id: ${id}`
      });

      return;
    }

    ControllerUtils.successfulOperation(res, {
      data: unidadAprendizaje
    });
  }

  static async update(req, res) {
    const { id } = req.params;

    const unidadAprendizaje = await UnidadAprendizaje.findByPk(id);

    if (!unidadAprendizaje) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la unidad de aprendizaje con el id: ${id}`
      });

      return;
    }

    const { nombre, idPlanEstudios } = req.body;

    try {
      const udAprendizajeActualizada = await unidadAprendizaje.update({
        nombre,
        idPlanEstudios
      });

      ControllerUtils.successfulOperation(res, {
        data: udAprendizajeActualizada
      });
    } catch (error) {
      // Error when updating `Unidad Aprendizaje`
      ControllerUtils.handleError(res, error);
    }
  }

  static async delete(req, res) {
    const { id } = req.params;

    const unidadAprendizaje = await UnidadAprendizaje.findByPk(id);

    if (!unidadAprendizaje) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la unidad de aprendizaje con el id: ${id}`
      });

      return;
    }

    try {
      await unidadAprendizaje.destroy();

      ControllerUtils.successfulOperation(res, {
        message: `La unidad de aprendizaje ${unidadAprendizaje.nombre} se eliminó correctamente`
      });
    } catch (error) {
      // Error when deleting `Unidad Aprendizaje`
      ControllerUtils.handleError(res, error);
    }
  }
}

export default UnidadAprendizajeController;
