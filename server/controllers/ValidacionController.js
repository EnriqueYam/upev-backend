import { Validacion } from '../models';
import { ControllerUtils } from '../utils';

class ValidacionController {
  static async list(req, res) {
    try {
      const validaciones = await Validacion.findAll(req.query.sql);

      ControllerUtils.successfulOperation(res, {
        data: validaciones
      });
    } catch (error) {
      ControllerUtils.handleError(res, error, {
        message: 'Ocurrió un error al ejecutar la consulta'
      });
    }
  }

  static async create(req, res) {
    const {
      cedulaRegistro,
      planeacionDidactica,
      firmaPlaneacionDidactica,
      fechaValidacion,
      usuarioValido,
      idSolicitud
    } = req.body;
    try {
      const nuevaValidacion = await Validacion.create({
        cedulaRegistro,
        planeacionDidactica,
        firmaPlaneacionDidactica,
        fechaValidacion,
        usuarioValido,
        idSolicitud
      });

      ControllerUtils.successfulInsertion(res, {
        message: `La validación se creó correctamente`,
        data: nuevaValidacion
      });
    } catch (error) {
      ControllerUtils.handleError(res, error);
    }
  }

  static async show(req, res) {
    const { id } = req.params;

    const validacion = await Validacion.findOne({
      where: {
        idValidacion: id
      },
      // Include its relationships
      include: ['solicitud']
    });

    if (!validacion) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la validación con el id: ${id}`
      });

      return;
    }

    ControllerUtils.successfulOperation(res, {
      data: validacion
    });
  }

  static async update(req, res) {
    const { id } = req.params;
    const validacion = await Validacion.findByPk(id);

    if (!validacion) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la validación con el id: ${id}`
      });
      return;
    }

    const {
      cedulaRegistro,
      planeacionDidactica,
      firmaPlaneacionDidactica,
      fechaValidacion,
      usuarioValido,
      idSolicitud
    } = req.body;

    try {
      const validacionActualizada = await validacion.update({
        cedulaRegistro,
        planeacionDidactica,
        firmaPlaneacionDidactica,
        fechaValidacion,
        usuarioValido,
        idSolicitud
      });

      ControllerUtils.successfulOperation(res, {
        data: validacionActualizada
      });
    } catch (error) {
      // Error when updating `Validacion`
      ControllerUtils.handleError(res, error);
    }
  }

  static async delete(req, res) {
    const { id } = req.params;

    const validacion = await Validacion.findByPk(id);

    if (!validacion) {
      ControllerUtils.notFoundError(res, {
        message: `No se encontró la validación con el id: ${id}`
      });
      return;
    }

    try {
      await validacion.destroy();

      ControllerUtils.successfulOperation(res, {
        message: `La validación se eliminó correctamente`
      });
    } catch (error) {
      // Error when deleting `Unidad Aprendizaje`
      ControllerUtils.handleError(res, error);
    }
  }
}
export default ValidacionController;
