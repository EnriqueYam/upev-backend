module.exports = {
  // eslint-disable-next-line no-unused-vars
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Turnados',
      [
        {
          estado: '1',
          tiempo: 10,
          idAreaUPEV: 1,
          createdAt: '2019-02-10',
          updatedAt: '2019-02-22'
        },
        {
          estado: '1',
          tiempo: 8,
          idAreaUPEV: 2,
          createdAt: '2019-02-10',
          updatedAt: '2019-02-22'
        }
      ],
      {}
    );
  },

  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Turnados', null, {});
  }
};
