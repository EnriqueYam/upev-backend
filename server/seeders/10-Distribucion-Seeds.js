module.exports = {
  // eslint-disable-next-line no-unused-vars
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Distribuciones',
      [
        {
          idSolicitud: 1,
          idTurnado: 1,
          createdAt: '2019-01-19',
          updatedAt: '2019-01-19'
        },
        {
          idSolicitud: 2,
          idTurnado: 1,
          createdAt: '2019-01-20',
          updatedAt: '2019-01-20'
        },
        {
          idSolicitud: 3,
          idTurnado: 1,
          createdAt: '2019-01-21',
          updatedAt: '2019-01-21'
        },
        {
          idSolicitud: 4,
          idTurnado: 1,
          createdAt: '2019-01-22',
          updatedAt: '2019-01-22'
        }
      ],
      {}
    );
  },

  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Distribuciones', null, {});
  }
};
