module.exports = {
  // eslint-disable-next-line no-unused-vars
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Observaciones',
      [
        {
          descripcion: 'Le falta informacion para considerar que es una salicitud valida',
          estado: 1,
          fecha: '2019-01-15',
          respuesta: 'Enterado, Corregire los errores lo mas pronto posible',
          fechaRespuesta: '2019-01-15',
          idDistribucion: 1,
          createdAt: '2019-01-15',
          updatedAt: '2019-01-15'
        },
        {
          descripcion: 'La validacion fue echa de manera correcta',
          estado: 1,
          fecha: '2019-01-20',
          respuesta: 'Enterado, Gracias por la aclaracion',
          fechaRespuesta: '2019-01-21',
          idDistribucion: 2,
          createdAt: '2019-01-20',
          updatedAt: '2019-01-22'
        },
        {
          descripcion: 'Esta validacion esta inhabilitada por el momento',
          estado: 1,
          fecha: '2019-01-22',
          respuesta: 'Enterado, Que podria hacer al respecto?',
          fechaRespuesta: '2019-01-22',
          idDistribucion: 3,
          createdAt: '2019-01-22',
          updatedAt: '2019-01-24'
        },
        {
          descripcion: 'Esta validacion ha expirado',
          estado: 1,
          fecha: '2019-01-24',
          respuesta: 'Enterado, Se lo informare a la escuela',
          fechaRespuesta: '2019-01-25',
          idDistribucion: 4,
          createdAt: '2019-01-24',
          updatedAt: '2019-01-26'
        }
      ],
      {}
    );
  },

  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Observaciones', null, {});
  }
};
