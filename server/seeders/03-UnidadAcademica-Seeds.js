module.exports = {
  // eslint-disable-next-line no-unused-vars
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'UnidadesAcademicas',
      [
        {
          nombre: 'Cecyt No.9',
          titular: 'Cristobal Moya',
          createdAt: '2019-01-14',
          updatedAt: '2019-04-10'
        },
        {
          nombre: 'ESCOM',
          titular: 'Sr. Benjamín Ríos',
          createdAt: '2019-08-14',
          updatedAt: '2019-10-11'
        },
        {
          nombre: 'Cecyt No.12',
          titular: 'Alicia Arredondo',
          createdAt: '2019-10-14',
          updatedAt: '2019-04-14'
        },
        {
          nombre: 'UPITA',
          titular: 'Rodrigo Estévez',
          createdAt: '2019-04-14',
          updatedAt: '2019-11-14'
        },
        {
          nombre: 'Cecyt No.3',
          titular: 'Catalina Córdova',
          createdAt: '2019-02-14',
          updatedAt: '2019-04-14'
        },
        {
          nombre: 'UPEV',
          titular: 'UPEV',
          createdAt: '2019-02-14',
          updatedAt: '2019-04-14'
        }
      ],
      {}
    );
  },

  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('UnidadesAcademicas', null, {});
  }
};
