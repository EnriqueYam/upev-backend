module.exports = {
  // eslint-disable-next-line no-unused-vars
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AreasUPEV',
      [
        {
          nombre: 'Asignación',
          createdAt: '2019-01-02',
          updatedAt: '2019-01-02'
        },
        {
          nombre: 'Evaluacion técnico',
          createdAt: '2019-01-02',
          updatedAt: '2019-01-02'
        },
        {
          nombre: 'Secretaria',
          createdAt: '2019-01-02',
          updatedAt: '2019-01-02'
        },
        {
          nombre: 'Director',
          createdAt: '2019-01-02',
          updatedAt: '2019-01-02'
        }
      ],
      {}
    );
  },

  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AreasUPEV', null, {});
  }
};
