module.exports = {
  // eslint-disable-next-line no-unused-vars
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'DatosContacto',
      [
        {
          tipo: 'Nombre Completo',
          valor: 'Enrique Eduardo',
          idUsuario: 1,
          createdAt: '2019-01-02',
          updatedAt: '2019-01-02'
        },
        {
          tipo: 'Domocilio',
          valor: 'Av. 5 de Mayo Col.Lomas de Trango',
          idUsuario: 1,
          createdAt: '2019-01-02',
          updatedAt: '2019-01-02'
        }
      ],
      {}
    );
  },

  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('DatosContacto', null, {});
  }
};
