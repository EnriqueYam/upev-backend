module.exports = {
  // eslint-disable-next-line no-unused-vars
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Participantes',
      [
        {
          idResponsable: 1,
          idSolicitud: 1,
          createdAt: '2019-01-15',
          updatedAt: '2019-01-20'
        },
        {
          idResponsable: 2,
          idSolicitud: 2,
          createdAt: '2019-01-17',
          updatedAt: '2019-01-18'
        },
        {
          idResponsable: 3,
          idSolicitud: 4,
          createdAt: '2019-02-20',
          updatedAt: '2019-02-20'
        },
        {
          idResponsable: 4,
          idSolicitud: 3,
          createdAt: '2019-03-21',
          updatedAt: '2019-03-21'
        }
      ],
      {}
    );
  },

  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Participantes', null, {});
  }
};
