module.exports = {
  // eslint-disable-next-line no-unused-vars
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Solicitudes',
      [
        {
          descripcion: 'Solicitud recibida en tiempo y forma',
          CADIDO: '09876543210987654321',
          folio: 1,
          numeroOficio: 32489,
          estado: '0',
          unidadAcademica: 'UPIITA',
          cedulaRegistro: 'www.google.com',
          planeacionDidactica: 'www.google.com',
          oficioSolicitud: 'www.google.com',
          dictamenAcademico: 'www.google.com',
          idPlataforma: 1,
          reporteOriginalidad: 'www.google.com',
          reporteEvaluacion: 'www.google.com',
          reporteTecPed: 'www.google.com',
          idResponsable: 1,
          idUnidadAprendizaje: 1,
          createdAt: '2019-01-15',
          updatedAt: '2019-01-20'
        },
        {
          descripcion: 'Solicitud de correcciones',
          CADIDO: '09876543210987654321',
          folio: 2,
          unidadAcademica: 'ESCOM',
          estado: '2',
          numeroOficio: 52456,
          cedulaRegistro: 'www.google.com',
          planeacionDidactica: 'www.google.com',
          oficioSolicitud: 'www.google.com',
          dictamenAcademico: 'www.google.com',
          idPlataforma: 1,
          reporteOriginalidad: 'www.google.com',
          reporteEvaluacion: 'www.google.com',
          reporteTecPed: 'www.google.com',
          idResponsable: 2,
          idUnidadAprendizaje: 2,
          createdAt: '2019-01-17',
          updatedAt: '2019-01-18'
        },
        {
          descripcion: 'Solicitud recibida en tiempo y forma',
          CADIDO: '09876543210987654321',
          folio: 3,
          estado: '1',
          unidadAcademica: 'VOCA 9',
          numeroOficio: 12345,
          cedulaRegistro: 'www.google.com',
          planeacionDidactica: 'www.google.com',
          oficioSolicitud: 'www.google.com',
          dictamenAcademico: 'www.google.com',
          idPlataforma: 1,
          reporteOriginalidad: 'www.google.com',
          reporteEvaluacion: 'www.google.com',
          reporteTecPed: 'www.google.com',
          idResponsable: 3,
          idUnidadAprendizaje: 3,
          createdAt: '2019-02-20',
          updatedAt: '2019-02-20'
        },
        {
          descripcion: 'Solicitud de correcciones',
          CADIDO: '09876543210987654321',
          folio: 4,
          estado: '2',
          unidadAcademica: 'UPIIBI',
          numeroOficio: 76543,
          cedulaRegistro: 'www.google.com',
          planeacionDidactica: 'www.google.com',
          oficioSolicitud: 'www.google.com',
          dictamenAcademico: 'www.google.com',
          idPlataforma: 1,
          reporteOriginalidad: 'www.google.com',
          reporteEvaluacion: 'www.google.com',
          reporteTecPed: 'www.google.com',
          idResponsable: 4,
          idUnidadAprendizaje: 4,
          createdAt: '2019-03-21',
          updatedAt: '2019-03-21'
        }
      ],
      {}
    );
  },

  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Solicitudes', null, {});
  }
};
