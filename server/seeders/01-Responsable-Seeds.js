module.exports = {
  // eslint-disable-next-line no-unused-vars
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Responsables',
      [
        {
          nombre: 'Diego Armando',
          puesto: 'Subdirector Academico',
          extension: '0454367',
          celular: '5565793245',
          correoInstitucional: 'ProfArmando@ipn.com.mx',
          correoAlternativo: 'DArmando@gmail.com',
          tipo: '0',
          createdAt: '2019-04-14',
          updatedAt: '2019-04-14'
        },
        {
          nombre: 'Enrique Guzman',
          puesto: 'Subgerente',
          extension: '0434354',
          celular: '5565793245',
          correoInstitucional: 'Enrq@ipn.com.mx',
          correoAlternativo: 'Enrique@gmail.com',
          tipo: '1',
          createdAt: '2019-05-09',
          updatedAt: '2019-07-10'
        },
        {
          nombre: 'Carlos Aguilar',
          puesto: 'Director Academico',
          extension: '0253567',
          celular: '5565793245',
          correoInstitucional: 'CarlosAg@ipn.com.mx',
          correoAlternativo: 'Carlos@gmail.com',
          tipo: '0',
          createdAt: '2019-07-22',
          updatedAt: '2019-10-22'
        },
        {
          nombre: 'Lalo Sanchez',
          puesto: 'Pasante',
          extension: '1454345',
          celular: '5565793245',
          correoInstitucional: 'LaloSanchez@ipn.com.mx',
          correoAlternativo: 'LaloLalo@gmail.com',
          tipo: '1',
          createdAt: '2019-09-20',
          updatedAt: '2019-11-05'
        }
      ],
      {}
    );
  },

  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Responsables', null, {});
  }
};
