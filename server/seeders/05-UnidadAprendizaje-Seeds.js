module.exports = {
  // eslint-disable-next-line no-unused-vars
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'UnidadesAprendizaje',
      [
        {
          nombre: 'Ingenieria De Software',
          idPlanEstudios: 1,
          createdAt: '2019-01-14',
          updatedAt: '2019-04-10'
        },
        {
          nombre: 'Calculo Aplicado',
          idPlanEstudios: 2,
          createdAt: '2019-01-14',
          updatedAt: '2019-04-10'
        },
        {
          nombre: 'Mineria de Datos',
          idPlanEstudios: 3,
          createdAt: '2019-01-14',
          updatedAt: '2019-04-10'
        },
        {
          nombre: 'IA',
          idPlanEstudios: 4,
          createdAt: '2019-01-14',
          updatedAt: '2019-04-10'
        }
      ],
      {}
    );
  },

  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('UnidadesAprendizaje', null, {});
  }
};
