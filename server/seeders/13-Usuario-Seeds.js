module.exports = {
  // eslint-disable-next-line no-unused-vars
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Usuarios',
      [
        {
          nombre: 'Enrique Alvarez',
          password: '$2b$12$Mvpsmy.cbfBXME30ay/rJuag4NM0ml6e91xcqUrl1EF7Czy5W35V2',
          correo: 'prueba@gmail.com',
          idRol: 1,
          idUnidadAcademica: 1,
          idAreaUPEV: 1,
          createdAt: '2019-08-14',
          updatedAt: '2019-10-11'
        }
      ],
      {}
    );
  },

  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Usuarios', null, {});
  }
};
