module.exports = {
  // eslint-disable-next-line no-unused-vars
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'PlanesEstudio',
      [
        {
          version: '0001-0002b',
          createdAt: '2019-04-14',
          updatedAt: '2019-06-14'
        },
        {
          version: '0002-0001s',
          createdAt: '2019-04-14',
          updatedAt: '2019-07-14'
        },
        {
          version: '0002-0002a',
          createdAt: '2019-04-14',
          updatedAt: '2019-09-14'
        },
        {
          version: '0002-0002e',
          createdAt: '2019-04-14',
          updatedAt: '2019-010-14'
        }
      ],
      {}
    );
  },

  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('PlanesEstudio', null, {});
  }
};
