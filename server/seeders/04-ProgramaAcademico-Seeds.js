module.exports = {
  // eslint-disable-next-line no-unused-vars
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'ProgramasAcademicos',
      [
        {
          nombre: 'ISC',
          idPlanEstudios: 1,
          idUnidadAcademica: 4,
          createdAt: '2019-02-10',
          updatedAt: '2019-04-14'
        },
        {
          nombre: 'Telematica',
          idPlanEstudios: 2,
          idUnidadAcademica: 3,
          createdAt: '2019-02-15',
          updatedAt: '2019-04-16'
        },
        {
          nombre: 'ISC',
          idPlanEstudios: 3,
          idUnidadAcademica: 2,
          createdAt: '2019-03-09',
          updatedAt: '2019-04-24'
        },
        {
          nombre: 'Telematica',
          idPlanEstudios: 4,
          idUnidadAcademica: 1,
          createdAt: '2019-04-01',
          updatedAt: '2019-04-14'
        }
      ],
      {}
    );
  },

  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ProgramasAcademicos', null, {});
  }
};
