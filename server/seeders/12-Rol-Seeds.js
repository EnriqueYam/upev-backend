module.exports = {
  // eslint-disable-next-line no-unused-vars
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Roles',
      [
        {
          nombre: 'Administrador',
          createdAt: '2019-01-02',
          updatedAt: '2019-01-02'
        },
        {
          nombre: 'UsuarioUPEV',
          createdAt: '2019-01-02',
          updatedAt: '2019-01-02'
        },
        {
          nombre: 'UsuarioEscuela',
          createdAt: '2019-01-02',
          updatedAt: '2019-01-02'
        }
      ],
      {}
    );
  },

  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Roles', null, {});
  }
};
