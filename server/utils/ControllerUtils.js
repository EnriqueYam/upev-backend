class ControllerUtils {
  static respond(response, responseCode, responseData) {
    response.status(responseCode).send(responseData);
  }

  static success(response, responseCode, responseData) {
    this.respond(response, responseCode, {
      success: true,
      ...responseData
    });
  }

  static error(response, responseCode, responseData) {
    this.respond(response, responseCode, {
      success: false,
      ...responseData
    });
  }

  static successfulOperation(response, responseData) {
    this.success(response, 200, responseData);
  }

  static successfulInsertion(response, responseData) {
    this.success(response, 201, responseData);
  }

  static buildError(error) {
    console.error(error);
    let errors;
    let message = 'No se pudieron almacenar los datos';
    let responseCode = 400;

    switch (error.name) {
      case ('SequelizeDatabaseError', 'SequelizeUniqueConstraintError'):
        errors = [error.original.sqlMessage];
        break;
      case 'SequelizeValidationError':
        errors = error.errors.map(e => e.message);
        break;

      default:
        responseCode = 500;
        message = 'Ocurrió un error inesperado';
        errors = [error.message];
        break;
    }

    return {
      responseCode,
      message,
      errors
    };
  }

  static handleError(response, error, defaultData) {
    const { responseCode, message, errors } = this.buildError(error);

    this.error(response, responseCode, {
      message,
      errors,
      ...defaultData
    });
  }

  static notFoundError(response, responseData) {
    this.error(response, 404, {
      ...responseData
    });
  }
}

export default ControllerUtils;
