import ExpressJWT from 'express-jwt';

const getTokenFromHeaders = req => {
  const { authorization } = req.headers;

  if (authorization && authorization.split(' ')[0] === 'Token') {
    return authorization.split(' ')[1];
  }

  return null;
};

const Auth = {
  required: ExpressJWT({
    secret: 'secret',
    userProperty: 'payload',
    getToken: getTokenFromHeaders
  }),
  optional: ExpressJWT({
    secret: 'secret',
    userProperty: 'payload',
    getToken: getTokenFromHeaders,
    credentialsRequired: false
  })
};

export default Auth;
