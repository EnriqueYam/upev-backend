import Sequelize from 'sequelize';

export default class Responsable extends Sequelize.Model {
  static init(sequelize) {
    return super.init(
      {
        idResponsable: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          allowNull: false,
          primaryKey: true
        },
        nombre: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo nombre es requerido'
            },
            notEmpty: {
              msg: 'El nombre del Responsable no puede estar vacío'
            },
            len: {
              args: [2, 45],
              msg: 'La longitud del nombre debe estar entre 2 y 45 caracteres'
            }
          }
        },
        puesto: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo puesto es requerido'
            },
            notEmpty: {
              msg: 'El puesto del Responsable no puede estar vacío'
            },
            len: {
              args: [2, 25],
              msg: 'La longitud del puesto debe estar entre 2 y 45 caracteres'
            }
          }
        },
        extension: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo extension es requerido'
            },
            notEmpty: {
              msg: 'La extensión del Responsable no puede estar vacía'
            },
            len: {
              args: [4, 7],
              msg: 'La longitud de la extension debe estar entre 4 y 7 caracteres'
            }
            // TODO: Check if isNumeric
          }
        },
        celular: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo celular es requerido'
            },
            notEmpty: {
              msg: 'El celular del Responsable no puede estar vacío'
            },
            len: {
              args: [8, 10],
              msg: 'La longitud del celular debe estar entre 8 y 10 caracteres'
            }
            // TODO: Check if isNumeric
          }
        },
        correoInstitucional: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo correo institucional es requerido'
            },
            notEmpty: {
              msg: 'El correo institucional del Responsable no puede estar vacío'
            },
            max: {
              args: 45,
              msg: 'La longitud máxima para el correo institucional es de 45 caracteres'
            },
            isEmail: {
              msg:
                'El formato del correo institucional no corresponde a un correo electrónico válido'
            }
          }
        },
        correoAlternativo: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo correo alternativo es requerido'
            },
            notEmpty: {
              msg: 'El correo alternativo del Responsable no puede estar vacío'
            },
            max: {
              args: 45,
              msg: 'La longitud máxima para el correo alternativo es de 45 caracteres'
            },
            isEmail: {
              msg: 'El formato del correo alternativo no corresponde a un correo electrónico válido'
            }
          }
        },
        tipo: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            // TODO: check if `tipo` can have a default value
            notNull: {
              msg: 'Debe especificarse el tipo de Responsable'
            },
            notEmpty: {
              msg: 'El valir para el tipo de Responsable no puede estar vacío'
            },
            len: {
              args: [1, 1],
              msg: 'La longitud para el tipo debe ser de 1 carácter'
            }
          }
        }
      },
      {
        tableName: 'Responsables',
        sequelize
      }
    );
  }

  static associate(models) {
    // Responsables <-> Solicitudes relationship
    Responsable.belongsToMany(models.Solicitud, {
      as: 'solicitudes',
      through: 'Participantes',
      foreignKey: 'idResponsable'
    });
  }
}
