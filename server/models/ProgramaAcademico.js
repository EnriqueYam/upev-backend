import Sequelize from 'sequelize';

export default class ProgramaAcademico extends Sequelize.Model {
  static init(sequelize) {
    return super.init(
      {
        idProgramaAcademico: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          allowNull: false,
          primaryKey: true
        },
        nombre: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true,
          validate: {
            notNull: {
              msg: 'El campo de Nombre es requerido'
            },
            notEmpty: {
              msg: 'El campo de Nombre del Programa Académico no puede estar vacío'
            },
            len: {
              args: [2, 50],
              msg:
                'La longitud del Nombre del Programa Académico debe estar entre 2 y 50 caracteres'
            }
          }
        },
        idPlanEstudios: {
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El id del Plan de Estudios es requerido'
            },
            notEmpty: {
              msg: 'El id del Plan de Estudios no puede estar vacío'
            }
          }
        },
        idUnidadAcademica: {
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El id de la Unidad Académica es requerido'
            },
            notEmpty: {
              msg: 'El id de la Unidad Acedémica no puede estar vacío'
            }
          }
        }
      },
      {
        tableName: 'ProgramasAcademicos',
        sequelize
      }
    );
  }

  static associate(models) {
    // ProgramasAcademicos -> PlanesEstudio relationship
    ProgramaAcademico.belongsTo(models.PlanEstudios, {
      foreignKey: 'idPlanEstudios',
      as: 'planEstudios'
    });

    // ProgramasAcademicos -> UnidadesAcademicas relationship
    ProgramaAcademico.belongsTo(models.UnidadAcademica, {
      foreignKey: 'idUnidadAcademica',
      as: 'unidadAcademica'
    });
  }
}
