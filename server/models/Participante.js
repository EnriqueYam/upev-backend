import Sequelize from 'sequelize';

export default class Participante extends Sequelize.Model {
  static init(sequelize) {
    return super.init(
      {
        idResponsable: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true
        },
        idSolicitud: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true
        }
      },
      {
        tableName: 'Participantes',
        sequelize
      }
    );
  }
}
