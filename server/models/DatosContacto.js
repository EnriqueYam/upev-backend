import Sequelize from 'sequelize';

export default class DatoContacto extends Sequelize.Model {
  static init(sequelize) {
    return super.init(
      {
        idDatoContacto: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        tipo: {
          allowNull: false,
          type: Sequelize.STRING,
          validate: {
            notNull: {
              msg: 'El campo Tipo es requerido'
            },
            notEmpty: {
              msg: 'El Tipo de Contacto no puede estar vacío'
            },
            len: {
              args: [2, 45],
              msg: 'La longitud del Tipo de Contacto debe estar entre 2 y 45 caracteres'
            }
          }
        },
        valor: {
          allowNull: false,
          type: Sequelize.STRING,
          validate: {
            notNull: {
              msg: 'El campo Valor es requerido'
            },
            notEmpty: {
              msg: 'El Valor no puede estar vacío'
            },
            len: {
              args: [2, 45],
              msg: 'La longitud del Valor debe estar entre 2 y 45 caracteres'
            }
          }
        },
        idUsuario: {
          type: Sequelize.INTEGER,
          references: {
            model: 'Usuarios',
            key: 'idUsuario'
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
        }
      },
      {
        tableName: 'DatoContactos',
        sequelize
      }
    );
  }

  static associate(models) {
    // UnidadesAprendizaje -> PlanesEstudio relationship
    DatoContacto.belongsTo(models.Usuario, {
      foreignKey: 'idUsuario',
      as: 'usuarios'
    });
  }
}
