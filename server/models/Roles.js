import Sequelize from 'sequelize';

export default class Rol extends Sequelize.Model {
  static init(sequelize) {
    return super.init(
      {
        idRol: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        nombre: {
          allowNull: false,
          type: Sequelize.STRING
        }
      },
      {
        tableName: 'Roles',
        sequelize
      }
    );
  }

  static associate(models) {
    // AreasUPEV <- Turnados
    Rol.hasMany(models.Usuario, {
      foreignKey: 'idRol',
      as: 'usuarios',
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE'
    });
  }
}
