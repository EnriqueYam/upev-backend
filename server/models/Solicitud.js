/* eslint-disable no-param-reassign */
import Sequelize from 'sequelize';

export default class Solicitud extends Sequelize.Model {
  static init(sequelize) {
    return super.init(
      {
        idSolicitud: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          allowNull: false,
          primaryKey: true
        },
        descripcion: {
          type: Sequelize.TEXT,
          allowNull: false,
          validate: {
            notNull: {
              msg: `El campo "descripción" es requerido`
            },
            notEmpty: {
              msg: 'El campo "descripción" no puede estar vacío'
            }
          }
        },
        CADIDO: {
          type: Sequelize.STRING,
          allowNull: true
          // validate: {
          //   notNull: {
          //     msg: 'El campo "CADIDO" es requerido'
          //   },
          //   notEmpty: {
          //     msg: 'El campo "CADIDO" no puede estar vacío'
          //   }
          // }
        },
        folio: {
          type: Sequelize.INTEGER,
          unique: true,
          allowNull: true
          // validate: {
          //   notNull: {
          //     msg: 'El campo "folio" es requerido'
          //   },
          //   notEmpty: {
          //     msg: 'El campo "folio" no puede estar vacío'
          //   },
          //   isNumeric: {
          //     msg: 'El folio solamente acepta caracteres numéricos'
          //   }
          // }
        },
        numeroOficio: {
          type: Sequelize.INTEGER,
          unique: true,
          allowNull: true
          // validate: {
          //   notNull: {
          //     msg: 'El campo "número de oficio" es requerido'
          //   },
          //   notEmpty: {
          //     msg: 'El campo "número de oficio" no puede estar vacío'
          //   },
          //   isNumeric: {
          //     msg: 'El numero de oficio solamente acepta caracteres númericos'
          //   }
          // }
        },
        estado: {
          type: Sequelize.TINYINT,
          // TODO: better handle of this
          defaultValue: 1, // Estado: En Edición
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo "estado" es requerido'
            },
            notEmpty: {
              msg: 'El campo "estado" no puede estar vacío'
            }
          }
        },
        cedulaRegistro: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo "cédula de registro" es requerido'
            },
            notEmpty: {
              msg: 'El campo "cédula de registro" no puede estar vacío'
            }
          }
        },
        planeacionDidactica: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo "planeacion didáctica" es requerido'
            },
            notEmpty: {
              msg: 'El campo "planeacion didáctica" no puede estar vacío'
            }
          }
        },
        oficioSolicitud: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo "oficio de solicitud" es requerido'
            },
            notEmpty: {
              msg: 'El campo "oficio de solicitud" no puede estar vacío'
            }
          }
        },
        dictamenAcademico: {
          type: Sequelize.STRING,
          allowNull: true
        },
        idPlataforma: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        reporteOriginalidad: {
          type: Sequelize.STRING,
          allowNull: true
        },
        reporteEvaluacion: {
          type: Sequelize.STRING,
          allowNull: true
        },
        solicitudEvaluacion: {
          type: Sequelize.STRING,
          allowNull: true
        },
        reporteTecPed: {
          type: Sequelize.STRING,
          allowNull: true
        },
        unidadAcademica: {
          type: Sequelize.STRING,
          allowNull: true
        },
        idResponsable: {
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El id del Responsable es requerido'
            },
            notEmpty: {
              msg: 'El id del Responsable no puede estar vacío'
            }
          }
        },
        idUnidadAprendizaje: {
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El id de la Unidad de Aprendizaje es requerido'
            },
            notEmpty: {
              msg: 'El id de la Unidad de Aprendizaje no puede estar vacío'
            }
          }
        }
      },
      {
        tableName: 'Solicitudes',
        sequelize,
        hooks: {
          beforeValidate(solicitud, options) {
            options.skip = [];
            // TODO: better handle of this
            if (solicitud.estado === 1) {
              const validateThis = [
                'idSolicitud',
                'estado',
                'idResponsable',
                'idUnidadAprendizaje'
              ];

              options.skip = Object.keys(Solicitud.rawAttributes).filter(
                attr => !validateThis.includes(attr)
              );
            }
          },
          async afterUpdate(solicitud) {
            if (
              solicitud.dictamenAcademico &&
              solicitud.solicitudEvaluacion &&
              solicitud.estado < 6
            ) {
              solicitud = await solicitud.update({ estado: 6 }, { hooks: false });
            }
          }
        }
      }
    );
  }

  static associate(models) {
    // Solicitudes -> Responsables relationship
    Solicitud.belongsTo(models.Responsable, {
      foreignKey: 'idResponsable',
      as: 'responsable'
    });

    // Solicitudes -> UnidadesAprendizaje relationship
    Solicitud.belongsTo(models.UnidadAprendizaje, {
      foreignKey: 'idUnidadAprendizaje',
      as: 'unidadAprendizaje'
    });

    // Solicitudes <-> Participantes relationship
    Solicitud.belongsToMany(models.Participante, {
      as: 'participantes',
      through: 'Participantes',
      foreignKey: 'idSolicitud'
    });

    // Solicitud <- Distribuciones relationship
    Solicitud.hasMany(models.Distribucion, {
      foreignKey: 'idSolicitud',
      as: 'distribuciones',
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE'
    });
  }
}
