import Sequelize from 'sequelize';

export default class UnidadAcademica extends Sequelize.Model {
  static init(sequelize) {
    return super.init(
      {
        idUnidadAcademica: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          allowNull: false,
          primaryKey: true
        },
        nombre: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo Nombre es requerido'
            },
            notEmpty: {
              msg: 'El Nombre de la Unidad Académica no puede estar vacío'
            },
            len: {
              args: [2, 45],
              msg:
                'La longitud del Nombre  de la Unidad Académica debe estar entre 2 y 45 caracteres'
            }
          }
        },
        titular: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo Titular es requerido'
            },
            notEmpty: {
              msg: 'El Titular de una Unidad Académica no puede estar vacío'
            },
            len: {
              args: [2, 45],
              msg:
                'La longitud del Titular de una Unidad Académica debe estar entre 2 y 45 caracteres'
            }
          }
        }
      },
      {
        tableName: 'UnidadesAcademicas',
        sequelize
      }
    );
  }

  static associate(models) {
    // UnidadesAcademicas <- Solicitudes relationship
    UnidadAcademica.hasMany(models.ProgramaAcademico, {
      foreignKey: 'idUnidadAcademica',
      as: 'programasAcademicos',
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE'
    });

    UnidadAcademica.belongsToMany(models.PlanEstudios, {
      as: 'planesEstudio',
      through: 'ProgramasAcademicos',
      foreignKey: 'idUnidadAcademica'
    });
  }
}
