import Sequelize from 'sequelize';
import bcrypt from 'bcrypt';
import JWT from 'jsonwebtoken';
import { promisify } from 'util';

const bcryptGenSaltAsync = promisify(bcrypt.genSalt);
const bcryptHashAsync = promisify(bcrypt.hash);
const SALT_ROUNDS = 12;

export default class Usuario extends Sequelize.Model {
  static init(sequelize) {
    return super.init(
      {
        idUsuario: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          allowNull: false,
          primaryKey: true
        },
        nombre: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo "nombre" es requerido'
            },
            notEmpty: {
              msg: 'El nombre de usuario no puede estar vacío'
            }
          }
        },
        correo: {
          type: Sequelize.STRING,
          allowNull: false
        },
        password: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo "contraseña" es requerido'
            },
            notEmpty: {
              msg: 'La contraseña del usuario no puede estar vacía'
            }
          }
        },
        idRol: {
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo "idRol" es requerido'
            },
            notEmpty: {
              msg: 'El "idRol" no puede ser vacío'
            }
          },
          references: {
            model: 'Roles',
            key: 'idRol'
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
        },
        idUnidadAcademica: {
          allowNull: false,
          type: Sequelize.INTEGER,
          validate: {
            notNull: {
              msg: 'El campo "idUnidadAcademica" es requerido'
            },
            notEmpty: {
              msg: 'El "idUnidadAcademica" no puede ser vacío'
            }
          },
          references: {
            model: 'UnidadesAcademicas',
            key: 'idUnidadAcademica'
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
        },
        idAreaUPEV: {
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo "idAreaUPEV" es requerido'
            },
            notEmpty: {
              msg: 'El "idAreaUPEV" no puede ser vacío'
            }
          },
          references: {
            model: 'AreasUPEV',
            key: 'idAreaUPEV'
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
        }
      },
      {
        tableName: 'Usuarios',
        sequelize
      }
    );
  }

  async hashPassword(password) {
    const salt = await bcryptGenSaltAsync(SALT_ROUNDS);
    const encryptedPassword = await bcryptHashAsync(password, salt);

    this.password = await encryptedPassword;
  }

  async validatePassword(password) {
    const validPassword = await bcrypt.compare(password, this.password);
    return validPassword;
  }

  generateJWT() {
    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate() + 30);

    return JWT.sign(
      {
        email: this.nombre,
        id: this.idUsuario,
        expires: parseInt(expirationDate.getTime() / 1000, 10)
      },
      'secret'
    );
  }

  static associate(models) {
    // Observaciones -> Solicitudes relationshs
    Usuario.belongsTo(models.UnidadAcademica, {
      foreignKey: 'idUnidadAcademica',
      as: 'unidadAcademica'
    });

    Usuario.belongsTo(models.Rol, {
      foreignKey: 'idRol',
      as: 'rol'
    });

    Usuario.belongsTo(models.AreaUPEV, {
      foreignKey: 'idAreaUPEV',
      as: 'areaupev'
    });
  }
}
