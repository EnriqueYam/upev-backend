import Sequelize from 'sequelize';

export default class AreaUPEV extends Sequelize.Model {
  static init(sequelize) {
    return super.init(
      {
        idAreaUPEV: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        nombre: {
          type: Sequelize.STRING
        }
      },
      {
        tableName: 'AreasUPEV',
        sequelize
      }
    );
  }

  static associate(models) {
    // AreasUPEV <- Turnados
    AreaUPEV.hasMany(models.Turnado, {
      foreignKey: 'idAreaUPEV',
      as: 'turnados',
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE'
    });
  }
}
