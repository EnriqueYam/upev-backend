import Sequelize from 'sequelize';
import config from '../config/config.json';
import * as models from './models-entry';

const env = process.env.NODE_ENV || 'development';
const currentConfig = config[env];

let sequelize;

if (currentConfig.use_env_variable) {
  sequelize = new Sequelize(process.env[currentConfig.use_env_variable], currentConfig);
} else {
  sequelize = new Sequelize(
    currentConfig.database,
    currentConfig.username,
    currentConfig.password,
    currentConfig
  );
}

const sequelizeModels = models;

Object.keys(sequelizeModels)
  .map(modelName => {
    const model = sequelizeModels[modelName];

    // Initiliaze every Model with a sequelize instance
    model.init(sequelize);

    return modelName;
  })
  .forEach(modelName => {
    const { associate } = sequelizeModels[modelName];

    if (typeof associate === 'function') {
      // Associate every model with the associations defined for it
      associate(sequelizeModels);
    }
  });

sequelizeModels.sequelize = sequelize;

module.exports = sequelizeModels;
