import Sequelize from 'sequelize';

export default class Distribucion extends Sequelize.Model {
  static init(sequelize) {
    return super.init(
      {
        idDistribucion: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        idSolicitud: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        idTurnado: {
          type: Sequelize.INTEGER,
          allowNull: false
        }
      },
      {
        tableName: 'Distribuciones',
        sequelize
      }
    );
  }

  static associate(models) {
    // Distribuciones -> Solicitudes relationship
    Distribucion.belongsTo(models.Solicitud, {
      foreignKey: 'idSolicitud',
      as: 'solicitud'
    });

    // Distribuciones -> Turnados relationship
    Distribucion.belongsTo(models.Turnado, {
      foreignKey: 'idTurnado',
      as: 'turnado'
    });

    // Distribuciones <- Observaciones relationship
    Distribucion.hasMany(models.Observacion, {
      foreignKey: 'idDistribucion',
      as: 'observaciones',
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE'
    });
  }
}
