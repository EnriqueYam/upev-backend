import Sequelize from 'sequelize';

export default class UnidadAprendizaje extends Sequelize.Model {
  static init(sequelize) {
    return super.init(
      {
        idUnidadAprendizaje: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          allowNull: false,
          primaryKey: true
        },
        nombre: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true,
          validate: {
            notNull: {
              msg: 'El campo Nombre es requerido'
            },
            notEmpty: {
              msg: 'El Nombre de la Unidad de Aprendizaje no puede estar vacío'
            },
            len: {
              args: [2, 45],
              msg:
                'La longitud del Nombre de la Unidad de Aprendizaje debe estar entre 2 y 45 caracteres'
            }
          }
        },
        idPlanEstudios: {
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El id del Plan de Estudios es requerido'
            },
            notEmpty: {
              msg: 'El id del Plan de Estudios no puede estar vacío'
            }
          }
        }
      },
      {
        tableName: 'UnidadesAprendizaje',
        sequelize
      }
    );
  }

  static associate(models) {
    // UnidadesAprendizaje -> PlanesEstudio relationship
    UnidadAprendizaje.belongsTo(models.PlanEstudios, {
      foreignKey: 'idPlanEstudios',
      as: 'planEstudios'
    });

    // UnidadesAprendizaje <- Solicitudes relationship
    UnidadAprendizaje.hasMany(models.Solicitud, {
      foreignKey: 'idUnidadAprendizaje',
      as: 'solicitudes',
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE'
    });
  }
}
