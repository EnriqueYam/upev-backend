import Sequelize from 'sequelize';

export default class PlanEstudios extends Sequelize.Model {
  static init(sequelize) {
    return super.init(
      {
        idPlanEstudios: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          allowNull: false,
          primaryKey: true
        },
        version: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true
        }
      },
      {
        tableName: 'PlanesEstudio',
        sequelize
      }
    );
  }

  static associate(models) {
    // PlanesEstudio <- ProgramasAcademicos relationship
    PlanEstudios.hasMany(models.ProgramaAcademico, {
      foreignKey: 'idPlanEstudios',
      as: 'programasAcademicos',
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE'
    });
    // PlanesEstudio <- UnidadesAprendizaje relationship
    PlanEstudios.hasMany(models.UnidadAprendizaje, {
      foreignKey: 'idPlanEstudios',
      as: 'unidadesAprendizaje',
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE'
    });

    PlanEstudios.belongsToMany(models.UnidadAcademica, {
      as: 'unidadesAcademicas',
      through: 'ProgramasAcademicos',
      foreignKey: 'idPlanEstudios'
    });
  }
}
