import Sequelize from 'sequelize';

export default class Turnado extends Sequelize.Model {
  static init(sequelize) {
    return super.init(
      {
        idTurnado: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        estado: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo de Estado es requerido'
            },
            notEmpty: {
              msg: 'El campo de Estado no puede estar vacío'
            },
            max: {
              args: 1,
              msg: 'La longitud del Estado debe de ser solamente de 1 caracter'
            }
          }
        },
        tiempo: {
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo del Tiempo es requerido'
            },
            notEmpty: {
              msg: 'El campo del Tiempo no puede estar vacío'
            },
            isNumeric: {
              msg: 'El campo deñ Tiempo solamente acepta caracteres numericos'
            }
          }
        },
        idAreaUPEV: {
          type: Sequelize.INTEGER,
          allowNull: false
        }
      },
      {
        tableName: 'Turnados',
        sequelize
      }
    );
  }

  static associate(models) {
    // Turnados <- Distribuciones relationship
    Turnado.hasMany(models.Distribucion, {
      foreignKey: 'idTurnado',
      as: 'distribuciones',
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE'
    });

    // Turnados -> AreasUPEV relationship
    Turnado.belongsTo(models.AreaUPEV, {
      foreignKey: 'idAreaUPEV',
      as: 'areaUPEV'
    });
  }
}
