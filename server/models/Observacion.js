import Sequelize from 'sequelize';

export default class Observacion extends Sequelize.Model {
  static init(sequelize) {
    return super.init(
      {
        idObservacion: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          allowNull: false,
          primaryKey: true
        },
        descripcion: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo de Descripción es requerido'
            },
            notEmpty: {
              msg: 'El campo de Descripción no puede estar vacío'
            },
            len: {
              args: [2, 200],
              msg: 'La longitud de la Descripción debe estar entre 2 y 200 caracteres'
            }
          }
        },
        estado: {
          type: Sequelize.INTEGER,
          defaultValue: 0,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo de Estado es requerido'
            },
            notEmpty: {
              msg: 'El campo de Estado no puede estar vacío'
            }
            // max: {
            //   args: 1,
            //   msg: 'La longitud del Estado debe de ser solamente de 1 caracter'
            // }
          }
        },
        fecha: {
          type: Sequelize.DATE,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo de la Fecha es requerido'
            },
            notEmpty: {
              msg: 'El campo de la Fecha no puede estar vacío'
            },
            isDate: {
              msg: 'El formato de la cadena ingresada no es de una fecha'
            }
          }
        },
        respuesta: {
          type: Sequelize.STRING,
          allowNull: true,
          validate: {
            notEmpty: {
              msg: 'El campo de respuesta no puede estar vacío'
            },
            len: {
              args: [2, 200],
              msg: 'La longitud de la respuesta debe estar entre 2 y 200 caracteres'
            }
          }
        },
        fechaRespuesta: {
          type: Sequelize.DATE,
          allowNull: true,
          validate: {
            notEmpty: {
              msg: 'El campo de la fecha de respuesta no puede estar vacío'
            },
            isDate: {
              msg: 'El formato de la cadena ingresada no es de una fecha'
            }
          }
        },
        idDistribucion: {
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: {
              msg: 'El campo del id de la Distribucion es requerido'
            },
            notEmpty: {
              msg: 'El campo del id de la Distribucion no puede estar vacío'
            }
          }
        }
      },
      {
        tableName: 'Observaciones',
        sequelize
      }
    );
  }

  static associate(models) {
    // Observaciones -> Solicitudes relationship
    Observacion.belongsTo(models.Distribucion, {
      foreignKey: 'idDistribucion',
      as: 'distribucion'
    });
  }
}
