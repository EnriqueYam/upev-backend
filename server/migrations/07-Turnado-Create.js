module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Turnados', {
      idTurnado: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      estado: {
        type: Sequelize.STRING,
        allowNull: false
      },
      tiempo: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      idAreaUPEV: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'AreasUPEV',
          key: 'idAreaUPEV'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Turnados');
  }
};
