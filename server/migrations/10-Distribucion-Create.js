module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('Distribuciones', {
        idDistribucion: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        idSolicitud: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'Solicitudes',
            key: 'idSolicitud'
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
        },
        idTurnado: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'Turnados',
            key: 'idTurnado'
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      })
      .then(() => {
        return queryInterface.addConstraint('Distribuciones', ['idSolicitud', 'idTurnado'], {
          type: 'unique',
          name: 'unique_distribucion_fks'
        });
      });
  },

  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Distribuciones');
  }
};
