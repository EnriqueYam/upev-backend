module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('UnidadesAprendizaje', {
      idUnidadAprendizaje: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nombre: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      idPlanEstudios: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'PlanesEstudio',
          key: 'idPlanEstudios'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('UnidadesAprendizaje');
  }
};
