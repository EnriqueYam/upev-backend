module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Solicitudes', {
      idSolicitud: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fechaRecepcion: {
        type: Sequelize.DATE
      },
      descripcion: {
        type: Sequelize.TEXT,
        allowNull: true,
        defaultValue: null
      },
      CADIDO: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: null
      },
      folio: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null,
        unique: true
      },
      numeroOficio: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null,
        unique: true
      },
      estado: {
        type: Sequelize.TINYINT,
        defaultValue: 1,
        allowNull: false
      },
      cedulaRegistro: {
        type: Sequelize.STRING,
        allowNull: true
      },
      planeacionDidactica: {
        type: Sequelize.STRING,
        allowNull: true
      },
      oficioSolicitud: {
        type: Sequelize.STRING,
        allowNull: true
      },
      dictamenAcademico: {
        type: Sequelize.STRING,
        allowNull: true
      },
      idPlataforma: {
        type: Sequelize.INTEGER
      },
      reporteOriginalidad: {
        type: Sequelize.STRING,
        allowNull: true
      },
      reporteEvaluacion: {
        type: Sequelize.STRING,
        allowNull: true
      },
      solicitudEvaluacion: {
        type: Sequelize.STRING,
        allowNull: true
      },
      reporteTecPed: {
        type: Sequelize.STRING,
        allowNull: true
      },
      unidadAcademica: {
        type: Sequelize.STRING,
        allowNull: true
      },
      idResponsable: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Responsables',
          key: 'idResponsable'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      idUnidadAprendizaje: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'UnidadesAprendizaje',
          key: 'idUnidadAprendizaje'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Solicitudes');
  }
};
