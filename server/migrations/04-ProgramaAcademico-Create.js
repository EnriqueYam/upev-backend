module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ProgramasAcademicos', {
      idProgramaAcademico: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nombre: {
        type: Sequelize.STRING,
        allowNull: false
      },
      idPlanEstudios: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'PlanesEstudio',
          key: 'idPlanEstudios'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      idUnidadAcademica: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'UnidadesAcademicas',
          key: 'idUnidadAcademica'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ProgramasAcademicos');
  }
};
