module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Participantes', {
      idResponsable: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        references: {
          model: 'Responsables',
          key: 'idResponsable'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      idSolicitud: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        references: {
          model: 'Solicitudes',
          key: 'idSolicitud'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Participantes');
  }
};
