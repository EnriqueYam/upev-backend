module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Observaciones', {
      idObservacion: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      descripcion: {
        type: Sequelize.STRING,
        allowNull: false
      },
      estado: {
        type: Sequelize.INTEGER,
        // allowNull: false,
        defaultValue: 0
      },
      fecha: {
        type: Sequelize.DATE,
        allowNull: false
      },
      respuesta: {
        type: Sequelize.STRING,
        allowNull: true
      },
      fechaRespuesta: {
        type: Sequelize.DATE,
        allowNull: true
      },
      idDistribucion: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Distribuciones',
          key: 'idDistribucion'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Observaciones');
  }
};
