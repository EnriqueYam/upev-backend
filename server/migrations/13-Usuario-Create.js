module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Usuarios', {
      idUsuario: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      correo: {
        allowNull: false,
        type: Sequelize.STRING
      },
      nombre: {
        allowNull: false,
        type: Sequelize.STRING
      },
      password: {
        allowNull: false,
        type: Sequelize.STRING
      },
      idRol: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'Roles',
          key: 'idRol'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      idUnidadAcademica: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'UnidadesAcademicas',
          key: 'idUnidadAcademica'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      idAreaUPEV: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'AreasUPEV',
          key: 'idAreaUPEV'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Usuarios');
  }
};
