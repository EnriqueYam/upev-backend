import Passport from 'passport';
import LocalStrategy from 'passport-local';
import { Usuario } from '../models';

Passport.use(
  new LocalStrategy(
    {
      usernameField: 'correo',
      passwordField: 'password'
    },
    async (correo, password, done) => {
      try {
        const usuario = await Usuario.findOne({
          where: {
            correo
          },
          include: ['unidadAcademica']
        });

        if (!(usuario && (await usuario.validatePassword(password)))) {
          return done(null, false, {
            message: 'Falló el inicio de sesión',
            errors: ['El correo o contraseña son incorrectos']
          });
        }

        return done(null, usuario);
      } catch (error) {
        return done(error);
      }
    }
  )
);
