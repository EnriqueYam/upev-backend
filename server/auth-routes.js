import { Router } from 'express';
import { AuthUtils } from './utils';
import { UsuarioController } from './controllers';

const AuthRoutes = Router();

// Regisrar un nuevo usuario
AuthRoutes.post('/registro', AuthUtils.optional, UsuarioController.create);
// Iniciar sesión
AuthRoutes.post('/login', AuthUtils.optional, UsuarioController.authenticate);
// Obtener al usuario actual (de la sesión)
AuthRoutes.get('/usuario-actual', AuthUtils.required, UsuarioController.current);

export default AuthRoutes;
