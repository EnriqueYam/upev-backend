import { Router } from 'express';
import {
  UnidadAcademicaController,
  UnidadAprendizajeController,
  SolicitudController,
  ProgramaAcademicoController,
  ResponsableController,
  PlanEstudiosController,
  ValidacionController,
  ObservacionController,
  TurnadoController,
  UsuarioController,
  DistribucionController
} from './controllers';
import { Query } from './middlewares';

const APIRoutes = Router();

// Unidad Academica Routes
const UnidadAcademicaRoute = '/unidades-academicas';
APIRoutes.get(UnidadAcademicaRoute, Query.parseQuery, UnidadAcademicaController.list);
APIRoutes.post(UnidadAcademicaRoute, UnidadAcademicaController.create);
APIRoutes.get(`${UnidadAcademicaRoute}/:id`, UnidadAcademicaController.show);
APIRoutes.put(`${UnidadAcademicaRoute}/:id`, UnidadAcademicaController.update);
APIRoutes.delete(`${UnidadAcademicaRoute}/:id`, UnidadAcademicaController.delete);

// Unidad Aprendizaje Routes
const UnidadAprendizajeRoute = '/unidades-aprendizaje';
APIRoutes.get(UnidadAprendizajeRoute, Query.parseQuery, UnidadAprendizajeController.list);
APIRoutes.post(UnidadAprendizajeRoute, UnidadAprendizajeController.create);
APIRoutes.get(`${UnidadAprendizajeRoute}/:id`, UnidadAprendizajeController.show);
APIRoutes.put(`${UnidadAprendizajeRoute}/:id`, UnidadAprendizajeController.update);
APIRoutes.delete(`${UnidadAprendizajeRoute}/:id`, UnidadAprendizajeController.delete);

// Solicitud Routes
const SolicitudRoute = '/solicitudes';
APIRoutes.get(SolicitudRoute, Query.parseQuery, SolicitudController.list);
APIRoutes.post(SolicitudRoute, SolicitudController.create);
APIRoutes.get(`${SolicitudRoute}/:id`, SolicitudController.show);
APIRoutes.put(`${SolicitudRoute}/:id`, SolicitudController.update);
APIRoutes.delete(`${SolicitudRoute}/:id`, SolicitudController.delete);

// Programa Academico Routes
const ProgramaAcademicoRoute = '/programas-academicos';
APIRoutes.get(ProgramaAcademicoRoute, Query.parseQuery, ProgramaAcademicoController.list);
APIRoutes.post(ProgramaAcademicoRoute, ProgramaAcademicoController.create);
APIRoutes.get(`${ProgramaAcademicoRoute}/:id`, ProgramaAcademicoController.show);
APIRoutes.put(`${ProgramaAcademicoRoute}/:id`, ProgramaAcademicoController.update);
APIRoutes.delete(`${ProgramaAcademicoRoute}/:id`, ProgramaAcademicoController.delete);

// Responsable Routes
const ResponsableRoute = '/responsables';
APIRoutes.get(ResponsableRoute, Query.parseQuery, ResponsableController.list);
APIRoutes.post(ResponsableRoute, ResponsableController.create);
APIRoutes.get(`${ResponsableRoute}/:id`, ResponsableController.show);
APIRoutes.put(`${ResponsableRoute}/:id`, ResponsableController.update);
APIRoutes.delete(`${ResponsableRoute}/:id`, ResponsableController.delete);

// PlanEstudios Routes
const PlanEstudiosRoute = '/planes-estudio';
APIRoutes.get(PlanEstudiosRoute, Query.parseQuery, PlanEstudiosController.list);
APIRoutes.post(PlanEstudiosRoute, PlanEstudiosController.create);
APIRoutes.get(`${PlanEstudiosRoute}/:id`, PlanEstudiosController.show);
APIRoutes.put(`${PlanEstudiosRoute}/:id`, PlanEstudiosController.update);
APIRoutes.delete(`${PlanEstudiosRoute}/:id`, PlanEstudiosController.delete);

// Validacion Routes
const ValidacionRoute = '/validaciones';
APIRoutes.get(ValidacionRoute, Query.parseQuery, ValidacionController.list);
APIRoutes.post(ValidacionRoute, ValidacionController.create);
APIRoutes.get(`${ValidacionRoute}/:id`, ValidacionController.show);
APIRoutes.put(`${ValidacionRoute}/:id`, ValidacionController.update);
APIRoutes.delete(`${ValidacionRoute}/:id`, ValidacionController.delete);

// Observacion Routes
const ObservacionRoutes = '/observaciones';
APIRoutes.get(ObservacionRoutes, Query.parseQuery, ObservacionController.list);
APIRoutes.post(ObservacionRoutes, ObservacionController.create);
APIRoutes.get(`${ObservacionRoutes}/:id`, ObservacionController.show);
APIRoutes.get(
  `${ObservacionRoutes}/distribucion/:idDistribucion`,
  ObservacionController.showOneObservation
);
APIRoutes.put(`${ObservacionRoutes}/respuesta/:id`, ObservacionController.updateRespuesta);
APIRoutes.put(`${ObservacionRoutes}/:id`, ObservacionController.update);
APIRoutes.delete(`${ObservacionRoutes}/:id`, ObservacionController.delete);

// Turnado Routes
const TurnadoRoute = '/turnados';
APIRoutes.get(TurnadoRoute, Query.parseQuery, TurnadoController.list);
APIRoutes.post(TurnadoRoute, TurnadoController.create);
APIRoutes.get(`${TurnadoRoute}/:id`, TurnadoController.show);
APIRoutes.put(`${TurnadoRoute}/:id`, TurnadoController.update);
APIRoutes.delete(`${TurnadoRoute}/:id`, TurnadoController.delete);

// Save Solicitud with Responsables
APIRoutes.post('/solicitudes-con-responsables', SolicitudController.createWithResponsables);

// User routes
const UserRoute = '/usuarios';
APIRoutes.get(UserRoute, Query.parseQuery, UsuarioController.list);
// routes.post(UserRoute, UsuarioController.create);

// CreateDistribución
const DistribucionRoutes = '/distribucion';
APIRoutes.post(DistribucionRoutes, DistribucionController.create);

export default APIRoutes;
