import parseData from 'odata-sequelize';
import URL from 'url';
import Sequelize from 'sequelize';

export default class Query {
  // Express Middleware to parse URL query
  static parseQuery(request, response, next) {
    const rawQuery = URL.parse(request.url).query;

    if (!rawQuery) {
      request.query.sql = {};
      return next();
    }

    const decodedQuery = decodeURI(rawQuery);

    let sqlQquery = {};
    try {
      sqlQquery = parseData(decodedQuery, Sequelize);

      if (request.query.$expand) {
        sqlQquery.include = request.query.$expand.split(',');
      }
    } catch (error) {
      const whereQuery = request.query;

      if (request.query.expand) {
        // Remove leading and trailing whitespaces
        const include = request.query.expand.split(',').map(s => s.trim());
        delete whereQuery.expand;

        sqlQquery.include = include;
      }

      sqlQquery.where = whereQuery;
    }

    request.query.sql = sqlQquery;
    return next();
  }
}
